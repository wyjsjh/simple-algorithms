#include "sort.h"
#include "search.h"

int factorial(int x, int ans)
{
    if(x == 1) return ans;
    printf("x:%d, ans:%d\n", x, ans);
    factorial(x-1, ans*x);
}

int euclidian(int a, int b)
{
    return a%b == 0 ? b : euclidian(b, a%b);
}

int sum(int a, int b)
{
    if(a == 0) return b;

    sum(a-1, b + a);
}

int main(int argc, char *argv[])
{
    uint8_t buf[9] = {5,1,7,8,6,4,3,2,9};

    printf("This is a test for bubble sort \n");
    bubble_sort(buf, 9);

    uint8_t buf_1[9] = {5,1,7,8,6,4,3,2,9};
    printf("This is a test for selection sort \n");
    selection_sort(buf_1, 9);

    uint8_t buf_2[9] = {5,1,4,5,6,4,3,2,9};
    printf("This is a test for insertion sort \n");
    insertion_sort(buf_2, 9);

    uint8_t buf_3[9] = {5,3,9,1,4,2,8,6,7};
    printf("This is a test for quick sort \n");
    quick_sort(buf_3, 9, 0, 8);

    uint8_t buf_4[9] = {5,3,9,1,4,2,8,6,7};
    printf("This is a test for linear search\n");
    uint8_t index = 0;
    bool is_found = linear_search(buf_4, 9, 2, &index);
    if(is_found){
        printf("found target at %d\n", index);
    }else{
        printf("target is not found\n");
    }

    printf("This is a test for binary search\n");
    is_found = binary_search(buf_4, 9, 6, &index);
      if(is_found){
        printf("found target at %d\n", index);
    }else{
        printf("target is not found\n");
    }  

    //printf("total is %d\n",factorial(5, 1));
    //printf("sum is %d\n", sum(100, 0));

    //printf("GCD of 111542 and 6953 is %d\n", euclidian(111542, 6953));

    return 0;
}