#ifndef __SORT_H__
#define __SORT_H__
#include <stdint.h>
#include <strings.h>
#include <stdio.h>


uint32_t bubble_sort(uint8_t *p_buf, uint32_t size);

uint32_t selection_sort(uint8_t *p_buf, uint32_t size);

uint32_t insertion_sort(uint8_t *p_buf, uint32_t size);

uint32_t quick_sort(uint8_t *p_buf, uint32_t size, uint32_t left, uint32_t right);

#endif