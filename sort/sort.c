#include "sort.h"

uint32_t bubble_sort(uint8_t *p_buf, uint32_t size)
{
    uint32_t steps  = 0;
    uint8_t data;
    for(uint32_t i = 0; i < size -1 ; i++)
    {
        for(uint32_t j = size -1; j > i; j--)
        {
            if(p_buf[j-1] > p_buf[j])
            {
                data = p_buf[j];
                p_buf[j] = p_buf[j-1];
                p_buf[j-1] = data; 
            }
            steps++;
            printf("Step %d: ",steps);
            for(uint32_t k = 0; k < size; k++)
            {
                printf("%d ", p_buf[k]);
            }
            printf("\n");
        }
    }
    return steps;
}

uint32_t selection_sort(uint8_t *p_buf, uint32_t size)
{
    uint32_t steps  = 0;
    uint8_t min;
    uint32_t index;

    for(uint32_t i = 0; i < size -1; i++)
    {
        min = 0xFF;
        for(uint32_t j = i; j < size - 1; j++)
        {
            if(min > p_buf[j]) 
            {
                min = p_buf[j];
                index = j;
            }
            steps++;
            printf("Step %d: ",steps);
            for(uint32_t k = 0; k < size; k++)
            {
                printf("%d ", p_buf[k]);
            }
            printf("\n");
        }
        p_buf[index] = p_buf[i];
        p_buf[i] = min;
    }
    return steps;
}

uint32_t insertion_sort(uint8_t *p_buf, uint32_t size)
{
    uint32_t steps  = 0;
    uint8_t data;
    for(uint32_t i = 1; i < size; i++)
    {
        data = p_buf[i];
        uint8_t j = i;
        while(j > 0 && p_buf[j-1] > data)
        {
            p_buf[j] = p_buf[j-1];
            j=j-1;
            steps++;
        }
        p_buf[j] = data; 
        printf("Step %d: ",steps);
        for(uint32_t k = 0; k < size; k++)
        {
            printf("%d ", p_buf[k]);
        }
        printf("\n");

    }
    return steps;
}


uint32_t quick_sort(uint8_t *p_buf, uint32_t size, uint32_t left, uint32_t right)
{
    static uint32_t steps = 0;
    if(left >= right)
    {
        steps++;
        printf("Step %d: ",steps);
        printf("\n");  
        return steps;
    }

    uint32_t  i = left;
    uint32_t  j = right;
    uint8_t key = p_buf[right];
    //printf("Key :%d\n",key);
    uint8_t data;

    while(i < j)
    {
        while(i < j && p_buf[i] < key)
        {
            //printf("Left i:%d, j:%d\n",i,j); 
            i++;  //2   6
        }
        
        while(i < j && p_buf[j] >= key)
        {
            //printf("Right i:%d, j:%d\n",i,j); 
            j--;  //7  6
        }
        data =  p_buf[i];
        p_buf[i] = p_buf[j];
        p_buf[j] = data;  
        //printf("Switch: i:%d, j:%d\n",i,j);   
    }
    p_buf[right] = p_buf[i];
    p_buf[i] = key;

    for(uint32_t k = 0; k < size; k++)
    {
        printf("%d ", p_buf[k]);
    }

    printf("\n");

    //printf("left:%d, right:%d\n", left,right);
    quick_sort(p_buf, size, left, i-1);

    quick_sort(p_buf, size, i+1, right);
}