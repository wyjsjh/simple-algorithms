#ifndef __SEARCH_H__
#define __SEARCH_H__

#include "sort.h"
#include <stdbool.h>

bool linear_search(uint8_t *p_buf, uint8_t size, uint8_t data,  uint8_t * const p_index);
bool binary_search(uint8_t *p_buf, uint8_t size, uint8_t data,  uint8_t * const p_index);

#endif