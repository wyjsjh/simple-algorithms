#include "search.h"

bool linear_search(uint8_t *p_buf, uint8_t size, uint8_t data, uint8_t * const p_index)
{
    bool rtn = false;
    for(uint8_t i = 0; i < size-1; i++)
    {
        if(p_buf[i] == data)
        {
            *p_index = i; 
            rtn = true;
            break;
        }
    }
    return rtn;
}

bool binary_search(uint8_t *p_buf, uint8_t size, uint8_t data, uint8_t * const p_index)
{
    insertion_sort(p_buf, size);

    bool found = false;

    uint8_t left = 0;
    uint8_t right = size-1;
    uint8_t middle;

    while(left <= right)
    {
        middle = (left + right)/2;
        
        if(p_buf[middle] < data)
        {
            left = middle + 1;
        }else if(p_buf[middle] > data)
        {
            right = middle - 1;
        }
        else{
            *p_index = middle;
            found = true; 
            break;
        }
    }
    return found;
}